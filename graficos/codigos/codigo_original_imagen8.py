import pandas as pd
import matplotlib.pyplot as plt 
import numpy as np

data = pd.read_excel('/home/andres/Desktop/pagina-web-cigefi/datos/Precipitacion/CIGEFI_lluvia_1982_2022.xlsx', sheet_name='Completo')

#Creamos una copia
data2 = data.copy()

#Creamos una nueva columna 'Date' a partir de las columna 'año', 'mes' y 'dia'
data2['Date'] = pd.to_datetime(data2['Año'].astype(str)+'-'+data2['Mes'].astype(str)+'-'+data2['Dia'].astype(str))
data2['Date'] = data2['Date'].astype(str)

#Eliminamos las columnas que no necesitamos
data2.pop('Año')
data2.pop('Mes')
data2.pop('Dia')

data2.Date = pd.to_datetime(data2.Date)
data2.set_index('Date',inplace=True)

data_mensual = []
data_mensual = pd.DataFrame(data_mensual)
data_mensual['Cant'] = data2.resample('M').count()
data_mensual['Lluvia'] = data2.resample('M').sum()

data_mensual['mes'] = (data_mensual.index).month

data_mensual_new = data_mensual[data_mensual['Cant'] > 18]

def new_df(data_mensual_new):
    
    if (data_mensual_new['mes'] == 1):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*31
    elif (data_mensual_new['mes'] == 2):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*28
    elif (data_mensual_new['mes'] == 3):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*31
    elif (data_mensual_new['mes'] == 4):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*30
    elif (data_mensual_new['mes'] == 5):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*31
    elif (data_mensual_new['mes'] == 6):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*30
    elif (data_mensual_new['mes'] == 7):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*31
    elif (data_mensual_new['mes'] == 8):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*31
    elif (data_mensual_new['mes'] == 9):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*30
    elif (data_mensual_new['mes'] == 10):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*31
    elif (data_mensual_new['mes'] == 11):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*30
    elif (data_mensual_new['mes'] == 12):
        return (data_mensual_new['Lluvia']/data_mensual_new['Cant'])*31
    
data_mensual_new['Lluvia'] = data_mensual_new.apply(new_df, axis = 1)

data_mensual_new.pop('Cant')

clim_mensual_prcp = []
clim_mensual_prcp = pd.DataFrame(clim_mensual_prcp)

#Obtenemos el 'Promedio' de la climatología, agrupando por mes
clim_mensual_prcp['mean'] = data_mensual_new.groupby(['mes']).mean()

#Obtenemos el 'Desviación Estándar' de la climatología, agrupando por mes
clim_mensual_prcp['desv'] = data_mensual_new.groupby(['mes']).std()

mas = clim_mensual_prcp["mean"] + clim_mensual_prcp["desv"]
menos = clim_mensual_prcp["mean"] - clim_mensual_prcp["desv"]

# Gráfico de barras de la climatología de precipitación (mm)
fig, ax = plt.subplots(figsize=[10,6])
ax.bar(clim_mensual_prcp.index,clim_mensual_prcp["mean"])
plt.title("Climatología de Precipitación - CIGEFI (84139)")
plt.ylabel("Precipitación (mm)")
plt.xlabel("Mes")

plt.savefig('/home/andres/Desktop/pagina-web-cigefi/graficos/imagen8.jpg', dpi = 300, bbox_inches = 'tight')

plt.show()