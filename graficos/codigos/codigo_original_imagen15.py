import pandas as pd
import matplotlib.pyplot as plt 
import numpy as np

data = pd.read_excel('/home/andres/Desktop/pagina-web-cigefi/datos/Precipitacion/CIGEFI_lluvia_1982_2022.xlsx', sheet_name='Completo')

data1 = data.dropna()

tabla = []
tabla = pd.DataFrame(tabla)
tabla['N'] = data1.groupby('Mes')['Lluvia'].count()#Cantidad de días por mes con valor

n = data1[data1['Lluvia'] > 0.1]

tabla['n'] = n.groupby('Mes')['Lluvia'].count()#Cantidad de días por mes con valor mayor o igual a 0.1

tabla['Porcentaje'] = (tabla['n']/tabla['N']) * 100

fig = plt.figure(figsize=(10,6))

plt.plot(tabla.index,tabla['Porcentaje'])

plt.title("Porcentaje días con lluvia (> 0.1mm) - CIGEFI (84139)")
plt.ylabel("Porcentaje (%)")
plt.xlabel("Mes")

plt.savefig('/home/andres/Desktop/pagina-web-cigefi/graficos/imagen15.jpg', dpi = 300, bbox_inches = 'tight')