import pandas as pd
import matplotlib.pyplot as plt 
import numpy as np

data = pd.read_excel('/home/andres/Desktop/pagina-web-cigefi/datos/Precipitacion/CIGEFI_lluvia_1982_2022.xlsx', sheet_name='Completo')

#Creamos una copia
data2 = data.copy()

#Creamos una nueva columna 'Date' a partir de las columna 'año', 'mes' y 'dia'
data2['Date'] = pd.to_datetime(data2['Año'].astype(str)+'-'+data2['Mes'].astype(str)+'-'+data2['Dia'].astype(str))
data2['Date'] = data2['Date'].astype(str)

#Eliminamos las columnas que no necesitamos
data2.pop('Año')
data2.pop('Mes')
data2.pop('Dia')

data2.Date = pd.to_datetime(data2.Date)
data2.set_index('Date',inplace=True)

data3 = data2.copy()

año_2001 = data3[pd.Timestamp('2001-01-01'):pd.Timestamp('2001-12-31')]
año_2015 = data3[pd.Timestamp('2015-01-01'):pd.Timestamp('2015-12-31')]
año_2012 = data3[pd.Timestamp('2012-01-01'):pd.Timestamp('2012-12-31')]
año_2022 = data3[pd.Timestamp('2022-01-01'):pd.Timestamp('2022-12-31')]
año_2010 = data3[pd.Timestamp('2010-01-01'):pd.Timestamp('2010-12-31')]
año_2017 = data3[pd.Timestamp('2017-01-01'):pd.Timestamp('2017-12-31')]
año_2008 = data3[pd.Timestamp('2008-01-01'):pd.Timestamp('2008-12-31')]
año_2002 = data3[pd.Timestamp('2002-01-01'):pd.Timestamp('2002-12-31')]

lista_años = [año_2001, año_2015, año_2012, año_2022, año_2010, año_2017, año_2008, año_2002]

lista_años_new = []
for i in lista_años:
    año = i.copy()
    año['mes'] = (año.index).month

    ene = (año.loc[(año.index).month == 1 ])
    feb = (año.loc[(año.index).month == 2 ])
    mar = (año.loc[(año.index).month == 3 ])
    abr = (año.loc[(año.index).month == 4 ])
    may = (año.loc[(año.index).month == 5 ])
    jun = (año.loc[(año.index).month == 6 ])
    jul = (año.loc[(año.index).month == 7 ])
    ago = (año.loc[(año.index).month == 8 ])
    set = (año.loc[(año.index).month == 9 ])
    oct = (año.loc[(año.index).month == 10 ])
    nov = (año.loc[(año.index).month == 11 ])
    dic = (año.loc[(año.index).month == 12 ])

    año_new = []
    año_new = pd.DataFrame(año_new)
    def new_df(año):

        if (año['mes'] == 1):
            return año.fillna(ene.sum()/ene.count())
        elif (año['mes'] == 2):
            return año.fillna(feb.sum()/feb.count())
        elif (año['mes'] == 3):
            return año.fillna(mar.sum()/mar.count())
        elif (año['mes'] == 4):
            return año.fillna(abr.sum()/abr.count())
        elif (año['mes'] == 5):
            return año.fillna(may.sum()/may.count())
        elif (año['mes'] == 6):
            return año.fillna(jun.sum()/jun.count())
        elif (año['mes'] == 7):
            return año.fillna(jul.sum()/jul.count())
        elif (año['mes'] == 8):
            return año.fillna(ago.sum()/ago.count())
        elif (año['mes'] == 9):
            return año.fillna(set.sum()/set.count())
        elif (año['mes'] == 10):
            return año.fillna(oct.sum()/oct.count())
        elif (año['mes'] == 11):
            return año.fillna(nov.sum()/nov.count())
        elif (año['mes'] == 12):
            return año.fillna(dic.sum()/dic.count())

    año_new = año.apply(new_df, axis = 1)
    año_new = año_new.reset_index()
    año_new.pop('Date')
    año_new.pop('mes')
    lista_años_new.append(año_new)

for i, df in enumerate(lista_años_new):
    lista_años_new[i] = lista_años_new[i].iloc[:,0]

años = pd.DataFrame(lista_años_new)

años = años.T

años.columns = ['2001', '2015', '2012', '2022', '2010', '2017', '2008', '2002']

años = años.cumsum()

años_new = años.fillna(method='ffill')

#Creamos el grafico
fig = plt.figure(figsize=(10,6))

plt.plot(años_new.index, años_new['2001'], label="2001")
plt.plot(años_new.index, años_new['2015'], label="2015")
plt.plot(años_new.index, años_new['2012'], label="2012")
plt.plot(años_new.index, años_new['2022'], label="2022")
plt.plot(años_new.index, años_new['2010'], label="2010")
plt.plot(años_new.index, años_new['2017'], label="2017")
plt.plot(años_new.index, años_new['2008'], label="2008")
plt.plot(años_new.index, años_new['2002'], label="1982-2022, Mediana", color="black")


#Damos formato
plt.title("Acumulado Anual Precipitación de 1982 al 2022 - CIGEFI (84139)")
plt.ylabel("Lluvia acumulada (mm)")
plt.xlabel("Día Juliano")

#Mostramos la leyenda
plt.legend()
plt.savefig('/home/andres/Desktop/pagina-web-cigefi/graficos/imagen16.jpg', dpi = 300, bbox_inches = 'tight')