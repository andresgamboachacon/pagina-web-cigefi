import pandas as pd
import plotly.graph_objects as go

# Leer los datos
data = pd.read_excel('/home/andres/Desktop/pagina-web-cigefi/datos/Precipitacion/CIGEFI_lluvia_1982_2022.xlsx', sheet_name='Completo')

# Eliminar filas con valores NaN
data1 = data.dropna()

# Calcular el número de días por mes con lluvia mayor a 0.1mm
tabla = pd.DataFrame()
tabla['N'] = data1.groupby('Mes')['Lluvia'].count()  # Cantidad de días por mes con valor
n = data1[data1['Lluvia'] > 0.1]
tabla['n'] = n.groupby('Mes')['Lluvia'].count()  # Cantidad de días por mes con valor mayor o igual a 0.1
tabla['Porcentaje'] = (tabla['n'] / tabla['N']) * 100

# Crear el gráfico interactivo con Plotly
fig = go.Figure()

# Definir el hovertemplate con formato personalizado
#hovertemplate = "Mes: %{x}<br>Porcentaje: %{y:.1f}%"

hovertemplate = {
    1: "Mes: Enero<br>Porcentaje: %{y:.1f}%",
    2: "Mes: Febrero<br>Porcentaje: %{y:.1f}%",
    3: "Mes: Marzo<br>Porcentaje: %{y:.1f}%",
    4: "Mes: Abril<br>Porcentaje: %{y:.1f}%",
    5: "Mes: Mayo<br>Porcentaje: %{y:.1f}%",
    6: "Mes: Junio<br>Porcentaje: %{y:.1f}%",
    7: "Mes: Julio<br>Porcentaje: %{y:.1f}%",
    8: "Mes: Agosto<br>Porcentaje: %{y:.1f}%",
    9: "Mes: Septiembre<br>Porcentaje: %{y:.1f}%",
    10: "Mes: Octubre<br>Porcentaje: %{y:.1f}%",
    11: "Mes: Noviembre<br>Porcentaje: %{y:.1f}%",
    12: "Mes: Diciembre<br>Porcentaje: %{y:.1f}%"
}

# Agregar la línea al gráfico
fig.add_trace(go.Scatter(x=tabla.index, y=tabla['Porcentaje'], mode='markers+lines', name='', hovertemplate=[hovertemplate.get(x, "Mes: %{x}<br>Porcentaje: %{y:.1f}%") for x in tabla.index], line=dict(color='#33739e')))

# Configurar el diseño del gráfico
fig.update_layout(title="<b>Porcentaje días con lluvia (> 0.1mm) - CIGEFI (84139)</b>",
                   title_x=0.5,
                   xaxis_title="Mes",
                   yaxis_title="Porcentaje (%)",
                   xaxis=dict(showline=True, linewidth=2, linecolor='black', mirror=True, tickfont=dict(size=16, color='black'), titlefont=dict(size=18, color='black'), range=[1, 12], ticks='outside', tickwidth=2),  # Ajustar tamaño de letra en el eje x
                   yaxis=dict(showline=True, linewidth=2, linecolor='black', mirror=True, tickfont=dict(size=16, color='black'), titlefont=dict(size=18, color='black'), range=[0, 100], ticks='outside', tickwidth=2),  # Ajustar tamaño de letra en el eje y
                   titlefont=dict(size=20, color='black')) # Ajustar tamaño de letra del título

# Guardar el gráfico como un archivo HTML interactivo
fig.write_html("/home/andres/Desktop/pagina-web-cigefi/graficos/imagen15.html")
