import pandas as pd
import plotly.graph_objects as go

data = pd.read_excel('/home/andres/Desktop/pagina-web-cigefi/datos/Precipitacion/CIGEFI_lluvia_1982_2022.xlsx', sheet_name='Completo')

# Creamos una copia
data2 = data.copy()

# Creamos una nueva columna 'Date' a partir de las columna 'año', 'mes' y 'dia'
data2['Date'] = pd.to_datetime(data2['Año'].astype(str)+'-'+data2['Mes'].astype(str)+'-'+data2['Dia'].astype(str))

# Eliminamos las columnas que no necesitamos
data2.drop(['Año', 'Mes', 'Dia'], axis=1, inplace=True)

# Reasignamos 'Date' como el índice
data2.set_index('Date', inplace=True)

# Resample y cálculo de datos mensuales
data_mensual = pd.DataFrame()
data_mensual['Cant'] = data2['Lluvia'].resample('M').count()
data_mensual['Lluvia'] = data2['Lluvia'].resample('M').sum()
data_mensual['mes'] = data_mensual.index.month

# Filtro por cantidad de datos
data_mensual_new = data_mensual[data_mensual['Cant'] > 18]

# Cálculo ajustado de precipitación
def new_df(row):
    days_in_month = pd.Timestamp(year=row.name.year, month=row.name.month, day=1).days_in_month
    return (row['Lluvia'] / row['Cant']) * days_in_month

data_mensual_new['Lluvia'] = data_mensual_new.apply(new_df, axis=1)

# Estadísticas mensuales
clim_mensual_prcp = pd.DataFrame()
clim_mensual_prcp['mean'] = data_mensual_new.groupby('mes')['Lluvia'].mean()
clim_mensual_prcp['desv'] = data_mensual_new.groupby('mes')['Lluvia'].std()

# Gráfico interactivo de barras de la climatología de precipitación (mm)
fig = go.Figure()

# Tiene la función de que el hovertemplate muestre el nombre del mes.

hovertemplate = {
    1: "Mes: Enero<br>Precipitación: %{y:.1f} mm",
    2: "Mes: Febrero<br>Precipitación: %{y:.1f} mm",
    3: "Mes: Marzo<br>Precipitación: %{y:.1f} mm",
    4: "Mes: Abril<br>Precipitación: %{y:.1f} mm",
    5: "Mes: Mayo<br>Precipitación: %{y:.1f} mm",
    6: "Mes: Junio<br>Precipitación: %{y:.1f} mm",
    7: "Mes: Julio<br>Precipitación: %{y:.1f} mm",
    8: "Mes: Agosto<br>Precipitación: %{y:.1f} mm",
    9: "Mes: Septiembre<br>Precipitación: %{y:.1f} mm",
    10: "Mes: Octubre<br>Precipitación: %{y:.1f} mm",
    11: "Mes: Noviembre<br>Precipitación: %{y:.1f} mm",
    12: "Mes: Diciembre<br>Precipitación: %{y:.1f} mm"
}

# Agregar la barra media
fig.add_trace(go.Bar(
    x=clim_mensual_prcp.index,
    y=clim_mensual_prcp['mean'],
    error_y=dict(type='data', array=clim_mensual_prcp['desv'], visible=False),
    marker=dict(color='rgb(55, 115, 158)'),
    name='',
    hovertemplate=[hovertemplate.get(x, "Mes: %{x}<br>Porcentaje: %{y:.1f}%") for x in clim_mensual_prcp.index]
))

# Configurar diseño del gráfico
fig.update_layout(
    title="<b>Climatología de Precipitación - CIGEFI (84139)</b>",
    title_x=0.5,
    xaxis_title="Mes",
    yaxis_title="Precipitación (mm)",
    xaxis=dict(showline=True, linewidth=2, linecolor='black', mirror=True, tickfont=dict(size=16, color='black'), titlefont=dict(size=18, color='black'), range=[0, 13], ticks='outside', tickwidth=2),
    yaxis=dict(showline=True, linewidth=2, linecolor='black', mirror=True, tickfont=dict(size=16, color='black'), titlefont=dict(size=18, color='black'), ticks='outside', tickwidth=2),
    titlefont=dict(size=20, color='black')
)

# Guardar el gráfico como un archivo HTML interactivo
fig.write_html("/home/andres/Desktop/pagina-web-cigefi/graficos/imagen8.html")
