import pandas as pd
import plotly.graph_objects as go

# Cargar datos y realizar procesamiento
data = pd.read_excel('/home/andres/Desktop/pagina-web-cigefi/datos/Precipitacion/CIGEFI_lluvia_1982_2022.xlsx', sheet_name='Completo')

data['Date'] = pd.to_datetime(data['Año'].astype(str) + '-' + data['Mes'].astype(str) + '-' + data['Dia'].astype(str))
data.set_index('Date', inplace=True)

# Seleccionar años de interés y calcular la suma acumulada
años_interes = ['2001', '2015', '2012', '2022', '2010', '2017', '2008', '2002']
df_años_interes_acumulado = data[data['Año'].astype(str).isin(años_interes)].groupby('Año').cumsum()

# Rellenar los valores NaN con el valor anterior
df_años_interes_acumulado.fillna(method='ffill', inplace=True)

# Colores para cada año
colores = ['#1f79b7', '#fb7d0e', '#2c9e2f', '#d52821', '#9166b6', '#85554e', '#d481bb', '#010101']

# Crear la figura de Plotly
fig = go.Figure()

# Agregar cada año como una línea en el gráfico
for i, año in enumerate(años_interes):
    año_data = df_años_interes_acumulado[df_años_interes_acumulado.index.year.astype(str) == año]
    fig.add_trace(go.Scatter(x=año_data.index.dayofyear, y=año_data['Lluvia'],
                             mode='lines', name=año, line=dict(color=colores[i])))
    
# Cambiar el nombre de la leyenda correspondiente al año 2002
fig.data[-1].name = '2002, Mediana'

# Configurar diseño del gráfico
fig.update_layout(
    title="<b>Acumulado Anual Precipitación de 1982 al 2022 - CIGEFI (84139)</b>",
    title_x=0.5,
    xaxis_title="Día Juliano",
    yaxis_title="Lluvia acumulada (mm)",
    xaxis=dict(showline=True, linewidth=2, linecolor='black', mirror=True, tickfont=dict(size=16, color='black'), titlefont=dict(size=18, color='black'), ticks='outside', tickwidth=2),  
    yaxis=dict(showline=True, linewidth=2, linecolor='black', mirror=True, tickfont=dict(size=16, color='black'), titlefont=dict(size=18, color='black'), ticks='outside', tickwidth=2), 
    legend=dict(x=0, y=1, xanchor='left', yanchor='top', font=dict(size=14)),
    titlefont=dict(size=20, color='black')
)

# Guardar el gráfico como un archivo HTML interactivo
fig.write_html("/home/andres/Desktop/pagina-web-cigefi/graficos/imagen16.html")

