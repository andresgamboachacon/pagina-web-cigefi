// Seleccionar elementos del HTML
const select = document.querySelector('#select');
const select2 = document.querySelector('#select2');
const opciones = document.querySelector('#opciones');
const opciones2 = document.querySelector('#opciones2');
const opciones3 = document.querySelector('#opciones3');
const opciones4 = document.querySelector('#opciones4');
const opciones5 = document.querySelector('#opciones5');
const opciones6 = document.querySelector('#opciones6');
const opciones7 = document.querySelector('#opciones7');
const contenidoSelect = document.querySelector('#select .contenido-select');
const contenidoSelect2 = document.querySelector('#select2 .contenido-select2');
var hiddenInput = document.querySelector('#inputSelect');
var hiddenInput2 = document.querySelector('#inputSelect2');
// Inicializar variables y limpiar valores anteriores
hiddenInput.value = "";
localStorage.removeItem('hiddenInputValue');
// Seleccionar elementos del HTML
var imagenElement = document.getElementById("imagen");
var iframeElement = document.getElementById("imagen_html");

var rutaImagen = "imagenes/imagen18.png";
const descripcion = document.getElementById('descripcion');
const tituloselect2 = document.querySelector('.contenido-select2 .titulo2');
// Agregar evento click a opciones del primer select.
document.querySelectorAll('#opciones > .opcion').forEach((opcion) => {
    opcion.addEventListener('click', (e) => {
        e.preventDefault();
        // Actualizar contenido y estado del primer select.
        contenidoSelect.innerHTML = e.currentTarget.innerHTML;
        select.classList.toggle('active');
        opciones.classList.toggle('active'); //Se despliegan las opciones del selector 1.
        hiddenInput.value = e.currentTarget.querySelector('.titulo-opcion').innerText;
        
        // Verificar opción seleccionada y en respuesta, ajustar imagen y descripción.
        if (hiddenInput.value === "Humedad relativa"){
            if (hiddenInput2.value !== "Distribución horaria mensual de la humedad relativa" && 
            hiddenInput2.value !=="Climatología de Humedad Relativa"){
                contenidoSelect2.innerHTML = tituloselect2.innerText;
                contenidoSelect2.className = "titulo2";
                descripcion.textContent = "";
                rutaImagen = "imagenes/imagen18.png"; //Nombre de la imagen en la carpeta.
            }
            // Actualizar imagen y descripción
            if (rutaImagen.endsWith('.html')) {
                imagenElement.style.display = 'none';
                iframeElement.style.display = 'block';
                iframeElement.src = rutaImagen;
            } else {
                iframeElement.style.display = 'none';
                imagenElement.style.display = 'block';
                imagenElement.src = rutaImagen;
            }
            
            // Agregar evento click a opciones del segundo select.
            document.querySelectorAll('#opciones2 > .opcion2').forEach((opcion2) => {
                opcion2.addEventListener('click', (e) => {
                    e.preventDefault();
                    // Actualizar contenido y estado del segundo select.
                    contenidoSelect2.innerHTML = e.currentTarget.innerHTML;
                    select2.classList.remove('active');
                    opciones2.classList.remove('active');
                    hiddenInput2.value = e.currentTarget.querySelector('.titulo-opcion2').innerText;
                    // Verificar opción seleccionada y en respuesta, ajustar imagen y descripción.
                    if (hiddenInput2.value === "Distribución horaria mensual de la humedad relativa"){
                        rutaImagen = "imagenes/imagen2.png";
                        descripcion.textContent = 'Ciclo anual (eje x) y diario (eje y) de los datos de humedad relativa de la estación automática del CIGEFI para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    else if (hiddenInput2.value === "Climatología de Humedad Relativa"){
                        rutaImagen = "imagenes/imagen13.jpg";
                        descripcion.textContent = 'Ciclo anual de humedad (%) y desviación estándar de la estación automática del CIGEFI (84139) para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    
                    // Actualizar imagen
                        
                    if (rutaImagen.endsWith('.html')) {
                        imagenElement.style.display = 'none';
                        iframeElement.style.display = 'block';
                        iframeElement.src = rutaImagen;
                    } else {
                        iframeElement.style.display = 'none';
                        imagenElement.style.display = 'block';
                        imagenElement.src = rutaImagen;
                    }
                });
            })
        };
        //Se repite todo lo anterior pero para otra variable.
        if (hiddenInput.value === "Precipitación"){
            document.querySelector('.carac-lluvias').style.display = 'block'; //Esto sirve para desplegar la imagen de lluvias
            if (hiddenInput2.value !== "Distribución horaria mensual de la precipitación" && 
            hiddenInput2.value !=="Climatología de Precipitación" && hiddenInput2.value !=="Porcentaje de dias con lluvia" &&
            hiddenInput2.value !=="Acumulado Anual Precipitación de 1982 al 2022"){
                contenidoSelect2.innerHTML = tituloselect2.innerText;
                contenidoSelect2.className = "titulo2";
                descripcion.textContent = "";
                rutaImagen = "imagenes/imagen18.png";
            }

            if (rutaImagen.endsWith('.html')) {
                imagenElement.style.display = 'none';
                iframeElement.style.display = 'block';
                iframeElement.src = rutaImagen;
            } else {
                iframeElement.style.display = 'none';
                imagenElement.style.display = 'block';
                imagenElement.src = rutaImagen;
            }

            document.querySelectorAll('#opciones3 > .opcion3').forEach((opcion3) => {
                opcion3.addEventListener('click', (e) => {
                    e.preventDefault();
                    contenidoSelect2.innerHTML = e.currentTarget.innerHTML;
                    select2.classList.remove('active');
                    opciones3.classList.remove('active');
                    hiddenInput2.value = e.currentTarget.querySelector('.titulo-opcion3').innerText;

                    if (hiddenInput2.value === "Distribución horaria mensual de la precipitación"){
                        rutaImagen = "imagenes/imagen3.jpg";
                        descripcion.textContent = 'Ciclo anual (eje x) y diario (eje y) de los datos de precipitación (mm) de la estación automática del CIGEFI para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    else if (hiddenInput2.value === "Climatología de Precipitación"){
                        rutaImagen = "graficos/imagen8.html";
                        descripcion.textContent = 'Ciclo anual del acumulado de precipitación mensual (mm) de la estación automática del CIGEFI (84139) para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    else if (hiddenInput2.value === "Porcentaje de dias con lluvia"){
                        rutaImagen = "graficos/imagen15.html";
                        descripcion.textContent = 'Ciclo anual del porcentaje de días con lluvia (%) de la estación automática del CIGEFI (84139) para el periodo de 1982 al 2022.';
                    }
                    else if (hiddenInput2.value === "Acumulado Anual Precipitación de 1982 al 2022"){
                        rutaImagen = "graficos/imagen16.html";
                        descripcion.textContent = 'Acumulado diario de la lluvia (mm) a lo largo del año en la estación automática del CIGEFI (84139) para el periodo del 1982 al 2022. El año 2002 corresponde a la mediana de los acumulados anuales, los años 2001 (mínimo), 2015 y 2012, fueron los años con menores acumulados anuales de los registros y los años 2022, 2010, 2017 y 2008 (máximo), los años que presentaron mayores acumulados anuales.';
                    }
                    
                    // Actualizar imagen
                    
                    if (rutaImagen.endsWith('.html')) {
                        imagenElement.style.display = 'none';
                        iframeElement.style.display = 'block';
                        iframeElement.src = rutaImagen;
                    } else {
                        iframeElement.style.display = 'none';
                        imagenElement.style.display = 'block';
                        imagenElement.src = rutaImagen;
                    }
                    
                });
            })
        }; 
        if (hiddenInput.value !== "Precipitación") {
            document.querySelector('.carac-lluvias').style.display = 'none'; //Esto sirve para esconder la imagen de lluvias.
        }
        //Se repite todo lo anterior pero para otra variable.
        if (hiddenInput.value === "Presión atmosférica"){
            
            if (hiddenInput2.value !== "Distribución horaria mensual de la presión atmosférica"){
                contenidoSelect2.innerHTML = tituloselect2.innerText;
                contenidoSelect2.className = "titulo2";
                descripcion.textContent = "";
                rutaImagen = "imagenes/imagen18.png";
            }
            if (rutaImagen.endsWith('.html')) {
                imagenElement.style.display = 'none';
                iframeElement.style.display = 'block';
                iframeElement.src = rutaImagen;
            } else {
                iframeElement.style.display = 'none';
                imagenElement.style.display = 'block';
                imagenElement.src = rutaImagen;
            }

            document.querySelectorAll('#opciones4 > .opcion4').forEach((opcion4) => {
                opcion4.addEventListener('click', (e) => {
                    e.preventDefault();
                    contenidoSelect2.innerHTML = e.currentTarget.innerHTML;
                    select2.classList.remove('active');
                    opciones4.classList.remove('active');
                    hiddenInput2.value = e.currentTarget.querySelector('.titulo-opcion4').innerText;

                    if (hiddenInput2.value === "Distribución horaria mensual de la presión atmosférica"){
                        rutaImagen = "imagenes/imagen4.jpg";
                        descripcion.textContent = 'Ciclo anual (eje x) y diario (eje y) de los datos de presión (hPa) de la estación automática del CIGEFI para el periodo del 26/10/2001 al 19/06/2019.';
                    }
                    
                    if (rutaImagen.endsWith('.html')) {
                        imagenElement.style.display = 'none';
                        iframeElement.style.display = 'block';
                        iframeElement.src = rutaImagen;
                    } else {
                        iframeElement.style.display = 'none';
                        imagenElement.style.display = 'block';
                        imagenElement.src = rutaImagen;
                    }

                });
            })
        }; 
        //Se repite todo lo anterior pero para otra variable.
        if (hiddenInput.value === "Radiación solar"){
            if (hiddenInput2.value !== "Distribución horaria mensual de la radiación solar global" &&
            hiddenInput2.value !== "Climatología Radiación Solar Global" ){
                contenidoSelect2.innerHTML = tituloselect2.innerText;
                contenidoSelect2.className = "titulo2";
                descripcion.textContent = "";
                rutaImagen = "imagenes/imagen18.png";
            }

            if (rutaImagen.endsWith('.html')) {
                imagenElement.style.display = 'none';
                iframeElement.style.display = 'block';
                iframeElement.src = rutaImagen;
            } else {
                iframeElement.style.display = 'none';
                imagenElement.style.display = 'block';
                imagenElement.src = rutaImagen;
            }

            document.querySelectorAll('#opciones5 > .opcion5').forEach((opcion5) => {
                opcion5.addEventListener('click', (e) => {
                    e.preventDefault();
                    contenidoSelect2.innerHTML = e.currentTarget.innerHTML;
                    select2.classList.remove('active');
                    opciones5.classList.remove('active');
                    hiddenInput2.value = e.currentTarget.querySelector('.titulo-opcion5').innerText;

                    if (hiddenInput2.value === "Distribución horaria mensual de la radiación solar global"){
                        rutaImagen = "imagenes/imagen5.jpg";
                        descripcion.textContent = 'Ciclo anual (eje x) y diario (eje y) de los datos de radiación solar global (MJ/m²) de la estación automática del CIGEFI para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    else if (hiddenInput2.value === "Climatología Radiación Solar Global"){
                        rutaImagen = "imagenes/imagen12.jpg";
                        descripcion.textContent = 'Ciclo anual de radiación solar global (MJ/m²) y desviación estándar de la estación automática del CIGEFI (84139) para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    
                    if (rutaImagen.endsWith('.html')) {
                        imagenElement.style.display = 'none';
                        iframeElement.style.display = 'block';
                        iframeElement.src = rutaImagen;
                    } else {
                        iframeElement.style.display = 'none';
                        imagenElement.style.display = 'block';
                        imagenElement.src = rutaImagen;
                    }

                });
            })
        }; 
        //Se repite todo lo anterior pero para otra variable.
        if (hiddenInput.value === "Temperatura"){
            if (hiddenInput2.value !== "Distribución horaria mensual de la temperatura" && 
            hiddenInput2.value !=="Climatología de Temperatura Promedio" && hiddenInput2.value !=="Climatología de Temperatura Máxima" &&
            hiddenInput2.value !=="Climatología de Temperatura Mínima"){
                contenidoSelect2.innerHTML = tituloselect2.innerText;
                contenidoSelect2.className = "titulo2";
                descripcion.textContent = "";
                rutaImagen = "imagenes/imagen18.png";
            }

            if (rutaImagen.endsWith('.html')) {
                imagenElement.style.display = 'none';
                iframeElement.style.display = 'block';
                iframeElement.src = rutaImagen;
            } else {
                iframeElement.style.display = 'none';
                imagenElement.style.display = 'block';
                imagenElement.src = rutaImagen;
            }

            document.querySelectorAll('#opciones6 > .opcion6').forEach((opcion6) => {
                opcion6.addEventListener('click', (e) => {
                    e.preventDefault();
                    contenidoSelect2.innerHTML = e.currentTarget.innerHTML;
                    select2.classList.remove('active');
                    opciones6.classList.remove('active');
                    hiddenInput2.value = e.currentTarget.querySelector('.titulo-opcion6').innerText;

                    if (hiddenInput2.value === "Distribución horaria mensual de la temperatura"){
                        rutaImagen = "imagenes/imagen6.jpg";
                        descripcion.textContent = 'Ciclo anual (eje x) y diario (eje y) de los datos de temperatura (°C) de la estación automática del CIGEFI para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    else if (hiddenInput2.value === "Climatología de Temperatura Promedio"){
                        rutaImagen = "imagenes/imagen9.jpg";
                        descripcion.textContent = 'Ciclo anual de la temperatura promedio (°C) y desviación estándar de la estación automática del CIGEFI (84139) para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    else if (hiddenInput2.value === "Climatología de Temperatura Máxima"){
                        rutaImagen = "imagenes/imagen10.jpg";
                        descripcion.textContent = 'Ciclo anual de la temperatura máxima (°C) y desviación estándar de la estación automática del CIGEFI (84139) para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    else if (hiddenInput2.value === "Climatología de Temperatura Mínima"){
                        rutaImagen = "imagenes/imagen11.jpg";
                        descripcion.textContent = 'Ciclo anual de la temperatura mínima (°C) y desviación estándar de la estación automática del CIGEFI (84139) para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    if (rutaImagen.endsWith('.html')) {
                        imagenElement.style.display = 'none';
                        iframeElement.style.display = 'block';
                        iframeElement.src = rutaImagen;
                    } else {
                        iframeElement.style.display = 'none';
                        imagenElement.style.display = 'block';
                        imagenElement.src = rutaImagen;
                    }
                });
            })
        }; 
        //Se repite todo lo anterior pero para otra variable.
        if (hiddenInput.value === "Viento"){
            document.querySelector('.rosa-container').style.display = 'grid';
            document.querySelector('.rosavientos').style.display = 'block';
            document.querySelector('.rosavientos2').style.display = 'block';
            if (hiddenInput2.value !== "Distribución horaria mensual del viento en superficie" &&
            hiddenInput2.value !== "Climatología de Velocidad del Viento en Superficie" ){
                contenidoSelect2.innerHTML = tituloselect2.innerText;
                contenidoSelect2.className = "titulo2";
                descripcion.textContent = "";
                rutaImagen = "imagenes/imagen18.png";
            }
            if (rutaImagen.endsWith('.html')) {
                imagenElement.style.display = 'none';
                iframeElement.style.display = 'block';
                iframeElement.src = rutaImagen;
            } else {
                iframeElement.style.display = 'none';
                imagenElement.style.display = 'block';
                imagenElement.src = rutaImagen;
            }

            document.querySelectorAll('#opciones7 > .opcion7').forEach((opcion7) => {
                opcion7.addEventListener('click', (e) => {
                    e.preventDefault();
                    contenidoSelect2.innerHTML = e.currentTarget.innerHTML;
                    select2.classList.remove('active');
                    opciones7.classList.remove('active');
                    hiddenInput2.value = e.currentTarget.querySelector('.titulo-opcion7').innerText;

                    if (hiddenInput2.value === "Distribución horaria mensual del viento en superficie"){
                        rutaImagen = "imagenes/imagen7.jpg";
                        descripcion.textContent = 'Ciclo anual (eje x) y diario (eje y) de los datos de velocidad, contornos y dirección del viento (m/s), flechas, de la estación automática del CIGEFI para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    else if (hiddenInput2.value === "Climatología de Velocidad del Viento en Superficie"){
                        rutaImagen = "imagenes/imagen14.jpg";
                        descripcion.textContent = 'Ciclo anual de la velocidad del viento promedio (m/s) y desviación estándar de la estación automática del CIGEFI (84139) para el periodo del 01/01/1999 al 19/06/2019.';
                    }
                    
                    if (rutaImagen.endsWith('.html')) {
                        imagenElement.style.display = 'none';
                        iframeElement.style.display = 'block';
                        iframeElement.src = rutaImagen;
                    } else {
                        iframeElement.style.display = 'none';
                        imagenElement.style.display = 'block';
                        imagenElement.src = rutaImagen;
                    }

                });
            })
        };
        if (hiddenInput.value !== "Viento") {
            document.querySelector('.rosa-container').style.display = 'none'; //Esto sirve para esconder la imagen de lluvias.
            document.querySelector('.rosavientos').style.display = 'none'; //Esto sirve para esconder rosa de los vientos.
            document.querySelector('.rosavientos2').style.display = 'none'; //Esto sirve para esconder rosa de los vientos.
        }

    });
});

select.addEventListener('click',() => { //Para que nunca coincidan abiertas las opciones del selector 1 y 2.
    select.classList.toggle('active'); //Si se abren unas opciones, se cierran las otras.
    opciones.classList.toggle('active');
    select2.classList.remove('active');
    opciones2.classList.remove('active');
    opciones3.classList.remove('active');
    opciones4.classList.remove('active');
    opciones5.classList.remove('active');
    opciones6.classList.remove('active');
    opciones7.classList.remove('active');
})

select2.addEventListener('click',() => {
    const hiddenInputValue = hiddenInput.value;

    if (hiddenInputValue == "") { //Impide que el selector 2 sea desplegado sin antes seleccionar en el selector 1.
        select2.classList.remove('active');
        opciones2.classList.remove('active');
        opciones3.classList.remove('active');
        opciones4.classList.remove('active');
        opciones5.classList.remove('active');
        opciones6.classList.remove('active');
        opciones7.classList.remove('active');
    }
    else if (hiddenInputValue !== "" && hiddenInput.value === "Humedad relativa" ) { //Las opciones del selector 2 dependen del 1
        select2.classList.toggle('active');
        opciones2.classList.toggle('active');
    }
    else if (hiddenInputValue !== "" && hiddenInput.value === "Precipitación" ) { //Las opciones del selector 2 dependen del 1
        select2.classList.toggle('active');
        opciones3.classList.toggle('active');
    }
    else if (hiddenInputValue !== "" && hiddenInput.value === "Presión atmosférica" ) {
        select2.classList.toggle('active');
        opciones4.classList.toggle('active');
    }
    else if (hiddenInputValue !== "" && hiddenInput.value === "Radiación solar" ) {
        select2.classList.toggle('active');
        opciones5.classList.toggle('active');
    }
    else if (hiddenInputValue !== "" && hiddenInput.value === "Temperatura" ) {
        select2.classList.toggle('active');
        opciones6.classList.toggle('active');
    }
    else if (hiddenInputValue !== "" && hiddenInput.value === "Viento" ) {
        select2.classList.toggle('active');
        opciones7.classList.toggle('active');
    }
    localStorage.removeItem('hiddenInputValue'); //Remueve valores que queden guardados para usos futuros.
})   

const caracteristicaslluvias = document.getElementsByClassName('carac-lluvias');

if (hiddenInput.value === "Precipitación") { //La tabla de Precipitación, unicamente se abre si se selecciona esta opción.
    for (let i = 0; i < caracteristicaslluvias.length; i++) {
        caracteristicaslluvias[i].style.display = 'block'; // Muestra el contenedor
    }
} else {
    for (let i = 0; i < caracteristicaslluvias.length; i++) {
        caracteristicaslluvias[i].style.display = 'none'; // Oculta el contenedor
    }
}

const rosavientos_contenedor = document.getElementsByClassName('rosa-container');
const rosavientos_titulo = document.getElementsByClassName('rosavientos');
const rosavientos_descripcion = document.getElementsByClassName('rosavientos2');

if (hiddenInput.value === "Viento") { //La tabla de Precipitación, unicamente se abre si se selecciona esta opción.
    for (let i = 0; i < rosavientos_contenedor.length; i++) {
        rosavientos_contenedor[i].style.display = 'grid'; // Muestra el contenedor
        rosavientos_titulo[i].style.display = 'block'; // Muestra el contenedor
        rosavientos_descripcion[i].style.display = 'block'; // Muestra el contenedor
    }
} else {
    for (let i = 0; i < rosavientos_contenedor.length; i++) {
        rosavientos_contenedor[i].style.display = 'none'; // Oculta el contenedor
        rosavientos_titulo[i].style.display = 'none'; // Oculta el contenedor
        rosavientos_descripcion[i].style.display = 'none'; // Oculta el contenedor
    }
}

// Esta parte está dedicada a agrandar las imagenes de la rosa de los vientos al dar un click
// Enero
document.getElementById('rosa-enero').addEventListener('click', function() {
    this.classList.toggle('rosa-enero-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-enero');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-enero-grande');
    }
});
// Febrero
document.getElementById('rosa-febrero').addEventListener('click', function() {
    this.classList.toggle('rosa-febrero-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-febrero');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-febrero-grande');
    }
});
// Marzo
document.getElementById('rosa-marzo').addEventListener('click', function() {
    this.classList.toggle('rosa-marzo-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-marzo');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-marzo-grande');
    }
});
// Abril
document.getElementById('rosa-abril').addEventListener('click', function() {
    this.classList.toggle('rosa-abril-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-abril');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-abril-grande');
    }
});
// Mayo
document.getElementById('rosa-mayo').addEventListener('click', function() {
    this.classList.toggle('rosa-mayo-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-mayo');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-mayo-grande');
    }
});
// Junio
document.getElementById('rosa-junio').addEventListener('click', function() {
    this.classList.toggle('rosa-junio-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-junio');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-junio-grande');
    }
});
// Julio
document.getElementById('rosa-julio').addEventListener('click', function() {
    this.classList.toggle('rosa-julio-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-julio');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-julio-grande');
    }
});
// Agosto
document.getElementById('rosa-agosto').addEventListener('click', function() {
    this.classList.toggle('rosa-agosto-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-agosto');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-agosto-grande');
    }
});
// Septiembre 
document.getElementById('rosa-septiembre').addEventListener('click', function() {
    this.classList.toggle('rosa-septiembre-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-septiembre');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-septiembre-grande');
    }
});
// Octubre
document.getElementById('rosa-octubre').addEventListener('click', function() {
    this.classList.toggle('rosa-octubre-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-octubre');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-octubre-grande');
    }
});
// Noviembre 
document.getElementById('rosa-noviembre').addEventListener('click', function() {
    this.classList.toggle('rosa-noviembre-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-noviembre');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-noviembre-grande');
    }
});
// Diciembre
document.getElementById('rosa-diciembre').addEventListener('click', function() {
    this.classList.toggle('rosa-diciembre-grande');
});

document.addEventListener('click', function(event) {
    const imagen1 = document.getElementById('rosa-diciembre');
    if (!imagen1.contains(event.target)) {
        imagen1.classList.remove('rosa-diciembre-grande');
    }
});


